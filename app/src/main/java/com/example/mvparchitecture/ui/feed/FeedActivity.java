package com.example.mvparchitecture.ui.feed;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.mvparchitecture.R;

public class FeedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);
    }
}
