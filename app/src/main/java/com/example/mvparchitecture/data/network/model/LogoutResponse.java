package com.example.mvparchitecture.data.network.model;

import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LogoutResponse {
    @Expose
    @SerializedName("status_code")
    private String statucCode;

    @Expose
    @SerializedName("message")
    private String message;

    public LogoutResponse(String statucCode, String message) {
        this.statucCode = statucCode;
        this.message = message;
    }

    public String getStatucCode() {
        return statucCode;
    }

    public void setStatucCode(String statucCode) {
        this.statucCode = statucCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if(this == obj) return true;
        if(obj == null || getClass() != obj.getClass()) return false;

        LogoutResponse that = (LogoutResponse) obj;

        if(statucCode !=null ? statucCode.equals(that.statucCode) : that.getClass()!=null)
            return false;
        return  message !=null ? message.equals(that.message) : that.getClass() ==null;

    }

    @Override
    public int hashCode() {
        int result = statucCode !=null ? statucCode.hashCode() : 0;
        result = 31 * result + (message !=null ? message.hashCode() :0);
        return result;
    }
}
