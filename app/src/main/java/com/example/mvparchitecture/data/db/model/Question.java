package com.example.mvparchitecture.data.db.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.ToMany;

import java.util.List;
@Entity(nameInDb = "questions")
public class Question {

    @Expose
    @SerializedName("id")
    @Id
    private Long id;

    @Expose
    @SerializedName("question_text")
    @Property(nameInDb = "question_text")
    private String questionText;

    @Expose
    @SerializedName("question_img_url")
    @Property(nameInDb = "question_img_url")
    private String imgUrl;

    @Expose
    @SerializedName("created_at")
    @Property(nameInDb = "created_at")
    private String createdAt;

    @Expose
    @SerializedName("updated_at")
    @Property(nameInDb = "updated_at")
    private String updatedAt;

    @ToMany(referencedJoinProperty = "questionId")
    private List<Option> optionList;

    /**
     * Used to resolve relations
     */




}
