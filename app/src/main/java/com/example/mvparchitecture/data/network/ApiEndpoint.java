package com.example.mvparchitecture.data.network;

import com.example.mvparchitecture.BuildConfig;
import com.google.gson.internal.JsonReaderInternalAccess;

public final class ApiEndpoint {
    public static final String ENDPOINT_GOOGLE_LOGIN = BuildConfig.BASE_URL
            + "/588d15d3100000ae072d2944";
    public static final String ENDPOINT_FACEBOOK_LOGIN = BuildConfig.BASE_URL +
            "/588d15d3100000ae072d2944";
    public static final String ENDPOINT_SERVER_LOGIN = BuildConfig.BASE_URL
            +"/588d15d3100000ae072d2944";
    public static final String ENDPOINT_LOGOUT = BuildConfig.BASE_URL
            +"/588d15d3100000ae072d2944";
    public static final String ENDPOINT_BLOG = BuildConfig.BASE_URL
            +"/588d15d3100000ae072d2944";
    public  static  final String ENDPOINT_OPEN_SOURCE = BuildConfig.BASE_URL
            +"588d15d3100000ae072d2944";
    private ApiEndpoint(){

    }
}
